import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cookieParser from 'cookie-parser'
import * as cors from 'cors'
import NipApi from "./api/NipApi";

const app = express();
const nipApi = new NipApi();

const whitelist = ['http://localhost:4300', 'http://10.0.0.34:4300'];
const corsOptions = {
    credentials: true,
    origin: (origin, callback) => {
        console.warn(`ORIGIN: ${origin}`);
        if (whitelist.includes(origin)) {
            callback(null, true);
        } else {
            callback(new Error('Not allowed by CORS'));
        }
    }
};

app.use(cookieParser());
app.use(cors(corsOptions));
app.use(bodyParser.json());

app.post('/nip', (req, res) => nipApi.nip(req, res));

app.listen(3000, () => {
    console.log('server started on port 3000')
});