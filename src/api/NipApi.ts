import * as express from 'express'
import * as request from 'request-promise'
import {parseString} from 'xml2js'
import CONFIG from "../../config/config";
import LogRequest from "../models/LogRequest";
import NipRequest from "../models/NipRequest";
import RegonRequest from "../models/RegonRequest";
import NipResult from "../models/NipResult";
import RegonResultF from "../models/RegonResultF";
import RegonResultP from "../models/RegonResultP";
import NipResponseData from "../models/NipResponseData";


export default class NipApi {

    private static readonly LOGIN_URI = 'https://wyszukiwarkaregon.stat.gov.pl/wsBIR/UslugaBIRzewnPubl.svc/ajaxEndpoint/Zaloguj';
    private static readonly NIP_SEARCH_URI = 'https://wyszukiwarkaregon.stat.gov.pl/wsBIR/UslugaBIRzewnPubl.svc/ajaxEndpoint/daneSzukaj';
    private static readonly REGON_SEARCH_URI = 'https://wyszukiwarkaregon.stat.gov.pl/wsBIR/UslugaBIRzewnPubl.svc/ajaxEndpoint/DanePobierzPelnyRaport';

    private readonly _options: any = {
        'jar': true,
        'pool': {
            'maxSockets': 1000
        },
        'headers': {
            'Accept-Language': 'en-US,en;q=0.8',
            'Accept': '*/*',
            'Content-Type': 'application/json; charset=utf-8',
            'Pragma': 'no-cache',
            'Origin': 'https://wyszukiwarkaregon.stat.gov.pl',
            'Host': 'wyszukiwarkaregon.stat.gov.pl',
            'DNT': '1',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'
        }
    };

    constructor() {
    }

    private static _handleError(message, res) {
        res.writeHead(400);
        res.write(JSON.stringify(message));
        res.end();
    }

    private static async _parseData(parsedBody): Promise<any> {
        if (!parsedBody) {
            return null;
        }
        // Convert parsedBody from XML to JS object
        return await new Promise((resolve, reject) => {
            parseString(JSON.parse(parsedBody).d, {
                normalizeTags: true,
                ignoreAttrs: true,
                mergeAttrs: true,
                explicitArray: false,
                explicitRoot: false
            }, (err, result) => {
                if (err || !result) {
                    reject(err || result);
                }
                resolve(result.dane);
            });
        });
    }

    public async nip(req: express.Request, res: express.Response) {
        let nip = null;
        // if (req.body.nip) {
        // const nipBody = req.body as NipBody;
        // nip = nipBody.nip;
        // } else {
        nip = '9721229364';
        // }
        console.log(`SEARCHING FOR NIP: ${nip}`);
        console.log(`SID: ${req.cookies.sid}`);
        if (!nip) {
            return NipApi._handleError({'error': true, 'message': 'MISSING_NIP'}, res);
        }

        // if (!req.cookies.sid) {
        // Retrieving SID
        await this._getSID(res);
        // }

        // Retrieving Data by NIP
        const nipResult: NipResult = await this._getDataByNIP(nip, res);
        console.log('nipResult', nipResult);
        const {nazwa, ulica, wojewodztwo, powiat, miejscowosc, regon, typ, kodpocztowy} = nipResult;

        const regonResult: RegonResultP | RegonResultF = await this._getFullReportByRegon(regon, typ, res);
        console.log('regonResult', regonResult);
        const {praw_adsiedznumernieruchomosci, praw_adsiedznumerlokalu} = regonResult as RegonResultP;
        const {fiz_adsiedznumernieruchomosci, fiz_adsiedznumerlokalu} = regonResult as RegonResultF;

        // Build responseData
        const street = ulica || miejscowosc;
        const buildingNumber = typ === 'P' ? praw_adsiedznumernieruchomosci : fiz_adsiedznumernieruchomosci;
        const localNumber = typ === 'P' ? praw_adsiedznumerlokalu.length || '' : fiz_adsiedznumerlokalu.length || '';
        let fullStreet = `${street} ${buildingNumber}`;
        if (localNumber) {
            fullStreet = fullStreet.concat(`/${localNumber}`);
        }

        const data: NipResponseData = {
            kod_pocztowy: kodpocztowy,
            miejscowosc,
            nazwa,
            nip,
            powiat,
            regon,
            ulica: fullStreet,
            wojewodztwo

        };
        res.send(data);
        // res.writeHead(response['statusCode'], {'Content-Type': 'application/json'});
        // res.write(JSON.stringify(response['data']));
        res.end();
    };

    /**
     * Retrieve Data by NIP
     * @param nip
     * @param res
     * @private
     */
    private _getDataByNIP(nip: string, res: express.Response): Promise<NipResult> {
        delete this._options.body;
        const gusRequest: NipRequest = {
            jestWojPowGmnMiej: true,
            pParametryWyszukiwania: {
                AdsSymbolGminy: null,
                AdsSymbolMiejscowosci: null,
                AdsSymbolPowiatu: null,
                AdsSymbolUlicy: null,
                AdsSymbolWojewodztwa: null,
                Dzialalnosci: null,
                FormaPrawna: null,
                Krs: null,
                Krsy: null,
                NazwaPodmiotu: null,
                Nip: nip,
                Nipy: null,
                NumerwRejestrzeLubEwidencji: null,
                OrganRejestrowy: null,
                PrzewazajacePKD: false,
                Regon: null,
                Regony9zn: null,
                Regony14zn: null,
                RodzajRejestru: null
            }
        };
        this._options.uri = NipApi.NIP_SEARCH_URI;
        this._options.body = JSON.stringify(gusRequest);
        return request.post(this._options)
            .then(r => {
                console.log('R', r);
                let _r = NipApi._parseData(r);
                if (!_r) {
                    throw 'EMPTY_NIP_RESULTS';
                }
                return _r;
            })
            .catch(e => {
                console.error(e);
                return NipApi._handleError({'error': true, 'message': 'CAN_NOT_GET_COMPANY'}, res);
            });

    }

    /**
     * Retrieve SID
     * @param res
     * @private
     */
    private _getSID(res: express.Response): Promise<string> {
        delete this._options.body;
        this._options.uri = NipApi.LOGIN_URI;
        const logRequest: LogRequest = {pKluczUzytkownika: CONFIG.USER_KEY};
        this._options.body = JSON.stringify(logRequest);
        return request.post(this._options)
            .then(r => {
                const sId = JSON.parse(r).d;
                if (!sId) {
                    throw 'EMPTY_LOGIN';
                }
                // Assign SID to cookies and to options, remove body from options
                res.cookie('sid', sId, {'maxAge': 30 * 60 * 1000, 'httpOnly': false});
                this._options.headers.sid = sId;
                delete this._options.body;
                return sId;
            })
            .catch(e => {
                console.error('ERROR', e);
                return NipApi._handleError({'error': true, 'message': 'CAN_NOT_LOGIN'}, res);
            });
    }

    /**
     * Retrieve full report
     * @param regon
     * @param type
     * @param res
     * @private
     */
    private _getFullReportByRegon(regon: string, type, res: express.Response): Promise<RegonResultF | RegonResultP> {
        delete this._options.body;
        this._options.uri = NipApi.REGON_SEARCH_URI;
        const value: RegonRequest = {
            pNazwaRaportu: type === 'P' ? 'PublDaneRaportPrawna' : 'PublDaneRaportDzialalnoscFizycznejCeidg',
            pRegon: regon
        };
        this._options.body = JSON.stringify(value);
        return request.post(this._options)
            .then(r => {
                let _r = NipApi._parseData(r);
                if (!_r) {
                    throw 'EMPTY_REGON_RESULTS';
                }
                return _r;
            })
            .catch(e => {
                console.error(e);
                return NipApi._handleError({'error': true, 'message': 'CAN_NOT_GET_COMPANY'}, res);
            });
    }


}

