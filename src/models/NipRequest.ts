export default interface NipRequest {

    jestWojPowGmnMiej: boolean;
    pParametryWyszukiwania: {
        AdsSymbolGminy: string,
        AdsSymbolMiejscowosci: string,
        AdsSymbolPowiatu: string,
        AdsSymbolUlicy: string,
        AdsSymbolWojewodztwa: string,
        Dzialalnosci: string,
        FormaPrawna: string,
        Krs: string,
        Krsy: string,
        NazwaPodmiotu: string,
        Nip: string,
        Nipy: string,
        NumerwRejestrzeLubEwidencji: string,
        OrganRejestrowy: null,
        PrzewazajacePKD: boolean,
        Regon: string,
        Regony9zn: string,
        Regony14zn: string,
        RodzajRejestru: string
    }

}