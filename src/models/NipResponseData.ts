export default interface NipResponseData {

    regon: string
    nip: string
    nazwa: string
    ulica: string
    miejscowosc: string
    kod_pocztowy: string
    powiat: string
    wojewodztwo: string

}