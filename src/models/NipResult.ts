export default interface NipResult {

    regon: string
    nazwa: string
    wojewodztwo: string
    powiat: string
    gmina: string
    miejscowosc: string
    kodpocztowy: string
    ulica: string
    typ: string
    silosid: string

}