export default interface RegonResultF {

    fiz_regon14: string,
    fiz_nip: string,
    fiz_nazwa: string,
    fiz_nazwaskrocona: string,
    fiz_numerwrejestrzeewidencji: string,
    fiz_datapowstania: string,
    fiz_datarozpoczeciadzialalnosci: string,
    fiz_datawpisudoregon: string,
    fiz_datazawieszeniadzialalnosci: string,
    fiz_datawznowieniadzialalnosci: string,
    fiz_datazaistnieniazmiany: string,
    fiz_datazakonczeniadzialalnosci: string,
    fiz_dataskresleniazregon: string,
    fiz_adsiedzkraj_symbol: string,
    fiz_adsiedzwojewodztwo_symbol: string,
    fiz_adsiedzpowiat_symbol: string,
    fiz_adsiedzgmina_symbol: string,
    fiz_adsiedzkodpocztowy: string,
    fiz_adsiedzmiejscowoscpoczty_symbol: string,
    fiz_adsiedzmiejscowosc_symbol: string,
    fiz_adsiedzulica_symbol: string,
    fiz_adsiedznumernieruchomosci: string,
    fiz_adsiedznumerlokalu: string,
    fiz_adsiedznietypowemiejscelokalizacji: string,
    fiz_numertelefonu: string,
    fiz_numerwewnetrznytelefonu: string,
    fiz_numerfaksu: string,
    fiz_adresemail: string,
    fiz_adresstronyinternetowej: string,
    fiz_adresemail2: string,
    fiz_adkorkraj_symbol: string,
    fiz_adkorwojewodztwo_symbol: string,
    fiz_adkorpowiat_symbol: string,
    fiz_adkorgmina_symbol: string,
    fiz_adkorkodpocztowy: string,
    fiz_adkormiejscowoscipoczty_symbol: string,
    fiz_adkormiejscowosc_symbol: string,
    fiz_adkorulica_symbol: string,
    fiz_adkornumernieruchomosci: string,
    fiz_adkornumerlokalu: string,
    fiz_adkornietypowemiejscelokalizacji: string,
    fiz_adkornazwapodmiotudokorespondencji: string,
    fiz_adsiedzkraj_nazwa: string,
    fiz_adsiedzwojewodztwo_nazwa: string,
    fiz_adsiedzpowiat_nazwa: string,
    fiz_adsiedzgmina_nazwa: string,
    fiz_adsiedzmiejscowosc_nazwa: string,
    fiz_adsiedzmiejscowoscpoczty_nazwa: string,
    fiz_adsiedzulica_nazwa: string,
    fiz_adkorkraj_nazwa: string,
    fiz_adkorwojewodztwo_nazwa: string,
    fiz_adkorpowiat_nazwa: string,
    fiz_adkorgmina_nazwa: string,
    fiz_adkormiejscowosc_nazwa: string,
    fiz_adkormiejscowoscpoczty_nazwa: string,
    fiz_adkorulica_nazwa: string,
    fiz_podstawowaformaprawna_symbol: string,
    fiz_szczegolnaformaprawna_symbol: string,
    fiz_formafinansowania_symbol: string,
    fiz_formawlasnosci_symbol: string,
    fiz_organzalozycielski_symbol: string,
    fiz_organrejestrowy_symbol: string,
    fiz_rodzajrejestruewidencji_symbol: string,
    fiz_podstawowaformaprawna_nazwa: string,
    fiz_szczegolnaformaprawna_nazwa: string,
    fiz_formafinansowania_nazwa: string,
    fiz_formawlasnosci_nazwa: string,
    fiz_organzalozycielski_nazwa: string,
    fiz_organrejestrowy_nazwa: string,
    fiz_rodzajrejestruewidencji_nazwa: string,
    fiz_jednosteklokalnych: string,
    fiz_datawpisudorejestruewidencji: string
}
